let firestore = firebase.firestore()

firebase.auth().onAuthStateChanged(user => {
    if (user) {
        current_user = user
        console.log('you are ' + current_user)
        let uid = current_user.uid
        firestore.collection("UserProfile").doc(uid).get().then(function (docs) {
            var profileData = docs.data()
            document.getElementsByClassName("username")[0].innerHTML = profileData.username
            document.getElementsByClassName("info")[0].innerHTML = profileData.description
            for (let i = 0; i < docs.data().followingPost.length; i++) {
                firestore.collection("AdoptablePost").doc(docs.data().followingPost[i]).get().then(function (doc) {
                    var colorClass = ''
                    // create post per doc
                    let lastbid = doc.data().bid
                    let startOrLast = ""
                    if (!lastbid.length) {
                        lastbid = doc.data().startingPrice
                        startOrLast = "Start Bid "
                    }
                    else {
                        lastbid = lastbid[lastbid.length - 1].bidPrice
                        startOrLast = "Last Bid "
                    }

                    let time = calculateExamRemainingTime(doc.data().endTime.toDate())

                    console.log(doc.data().endTime.toDate())

                    function calculateExamRemainingTime(exam_end_at) {


                        const exam_ending_at = exam_end_at;
                        const current_time = new Date();

                        const totalSeconds = Math.floor((exam_ending_at - (current_time)) / 1000);;
                        const totalMinutes = Math.floor(totalSeconds / 60);
                        const totalHours = Math.floor(totalMinutes / 60);
                        const totalDays = Math.floor(totalHours / 24);

                        const days = totalDays
                        const hours = totalHours - (totalDays * 24);
                        const minutes = totalMinutes - (totalDays * 24 * 60) - (hours * 60);
                        const seconds = totalSeconds - (totalDays * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);

                        //const examRemainingHoursSection = document.querySelector('#remainingHours');
                        //const examRemainingMinutesSection = document.querySelector('#remainingMinutes');
                        //const examRemainingSecondsSection = document.querySelector('#remainingSeconds');

                        const examRemainingDaysSection = days.toString()
                        const examRemainingHoursSection = hours.toString();
                        const examRemainingMinutesSection = minutes.toString();
                        const examRemainingSecondsSection = seconds.toString();
                        if (exam_ending_at <= current_time || doc.data().autoBuyPrice <= lastbid) {
                            colorClass = 'red'
                            return "Auction End"
                        } else {
                            colorClass = 'green'
                            if (examRemainingDaysSection > 0) {
                                return examRemainingDaysSection + " Days "
                            }
                            else if (examRemainingHoursSection > 0) {
                                return examRemainingHoursSection + " Hours "
                            }
                            else if (examRemainingMinutesSection > 0) {
                                return examRemainingMinutesSection + " Minutes "
                            }
                            else if (examRemainingSecondsSection >= 0) {
                                return "less than 1 minute"
                            }
                        }

                    }

                    let hiddenButtonState = 'block'
                    if (time === 'Auction End') {
                        hiddenButtonState = 'none'
                    }
                    let fastBidPrice = lastbid + doc.data().increasePrice
                    if (lastbid + doc.data().increasePrice > doc.data().autoBuyPrice) {
                        fastBidPrice = doc.data().autoBuyPrice
                    }

                    document.getElementsByClassName("fav")[0].innerHTML =
                    `<div class="post" style="margin: 0 0 5%";>
                        <div class="split-row">
                            <div id="timeOf` + doc.id + `" class="time ` + colorClass + `">` + time + `</div>
                            <div id="lastBidOf` + doc.id + `" class="lastBit">` + startOrLast + lastbid + ` ฿</div>
                        </div>
                        <div class="image" id="`+ doc.id + `img" style="background-image: url();" onclick="goToPost('` + doc.id + `')"></div>
                        <div class="split-row">
                            <div class="split-col">
                                <div class="name">`+ doc.data().name + `</div>
                                <div class="price"> Auto Buy ` + doc.data().autoBuyPrice + ` ฿ </div>
                            </div>
                            <div class="bid">
                                <button id="fastBidButtonOf` + doc.id + `" class="bidbtn" style="display: ` + hiddenButtonState + `" onclick="fastBidding('` + doc.id + `', ` + (fastBidPrice) + `)">Bid <br>` + (fastBidPrice) + ` ฿ </button>
                            </div>
                        </div>
                        <div class="split-row">
                            <div id="messageOf` + doc.id + `"></div>
                        </div>
                    </div>`
                    + document.getElementsByClassName("fav")[0].innerHTML
                    getImageUrl(doc.data().mainImgName)
                    function getImageUrl(name) { //put ImgName from firebase into 'name' parameter
                        let imgUrl
                        firebase.storage().ref(name).getDownloadURL().then(function (url) {
                            document.getElementById(doc.id + "img").style.backgroundImage = "url(" + url + ")"
                        });
                    }
                });
            }
        })
    } else {
        console.log('you do not login')
        window.location.href = "/public/login.html";
    }
});

function fastBidding(id, bidValue) {
    if (current_user === '') {
        window.location.href = "/public/login.html";
        return
    }
    let errorMessage = ''
    let currentBidPrice = bidValue
    let DATA
    firestore.collection("AdoptablePost").doc(id).onSnapshot(function (docs) {
        DATA = docs.data()
        if (!DATA.bid.length) {
            if (currentBidPrice >= DATA.startingPrice) {
                updateBid()
            } else {
                errorMessage = errorMessage + 'Please bid more than starting price'
            }
        } else {
            if (currentBidPrice < DATA.bid[DATA.bid.length - 1].bidPrice + DATA.increasePrice && currentBidPrice < DATA.autoBuyPrice) {
                errorMessage = errorMessage + "Please bid more than last bid plus increase price"
            } else if (DATA.bid[DATA.bid.length - 1].bidPrice >= DATA.autoBuyPrice) {
                errorMessage = errorMessage + "This Auction is already end"
            }
        }

        if (errorMessage === '') {
            updateBid()
        }
    });

    document.getElementById("messageOf" + id).innerHTML = errorMessage
    document.getElementById("messageOf" + id).style.color = 'red'

    function updateBid() {
        let bidObj = {
            uid: current_user.uid,
            bidPrice: parseInt(currentBidPrice),
            date: firebase.firestore.Timestamp.fromDate(new Date())
        }
        let myBidArray = [...DATA.bid]
        console.log(myBidArray)
        myBidArray.push(bidObj)
        console.log(myBidArray)
        firestore.collection("AdoptablePost").doc(id).set({
            bid: myBidArray
        }, { merge: true });
        document.getElementById("messageOf" + id).style.color = 'green'
        document.getElementById("messageOf" + id).innerHTML = "Bid Success"
        fastUpdateFollowingPost(id)
        setTimeout(function () {
            document.getElementById("messageOf" + id).innerHTML = ""
        }, 3000);
    }
}
function fastUpdateFollowingPost(id) {
    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            current_user = user
            firestore.collection("UserProfile").doc(user.uid).get().then(function (docs) {
                var profileData = docs.data()
                var tempArray = [...profileData.followingPost]
                if (!tempArray.includes(id)) {
                    tempArray.push(id)
                    firestore.collection("UserProfile").doc(user.uid).set({
                        followingPost: tempArray
                    }, { merge: true });
                }
            })
        }
    })
    firestore.collection("AdoptablePost").doc(id).onSnapshot(function (docs) { //refresh of fastBid
        let DATA = docs.data()
        let nextBidPrice = DATA.bid[DATA.bid.length - 1].bidPrice + DATA.increasePrice
        document.getElementById("lastBidOf" + id).innerHTML = "Last Bid " + DATA.bid[DATA.bid.length - 1].bidPrice + " ฿"
        if (DATA.bid[DATA.bid.length - 1].bidPrice + DATA.increasePrice > DATA.autoBuyPrice) {
            nextBidPrice = DATA.autoBuyPrice
        }
        document.getElementById("fastBidButtonOf" + id).innerHTML = "Bid <br>" + nextBidPrice + " ฿"
        document.getElementById("fastBidButtonOf" + id).setAttribute('onclick', "fastBidding('" + id + "', " + nextBidPrice + ")")
        if (DATA.bid[DATA.bid.length - 1].bidPrice >= DATA.autoBuyPrice) {
            document.getElementById("fastBidButtonOf" + id).style.display = "none"
            document.getElementById("timeOf" + id).innerHTML = "Auction End"
            document.getElementById("timeOf" + id).classList.remove('green')
            document.getElementById("timeOf" + id).classList.add('red')
        }
    });
}

function goToPost(name) {
    sessionStorage.setItem("postDocId", name);
    window.location.href = "auctionPost.html"
}