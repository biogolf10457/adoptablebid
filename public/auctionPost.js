let firestore = firebase.firestore()
let DATA

var postDocId = sessionStorage.getItem("postDocId");
//var postDocId = "bdfsi676841"

firestore.collection("AdoptablePost").doc(postDocId).onSnapshot(function(docs){
    console.log(docs.data());
    DATA = docs.data()
    //set data to html
    document.getElementById("adoptName").innerHTML = DATA.name
    document.getElementById("adoptSB").innerHTML = DATA.startingPrice
    document.getElementById("adoptIB").innerHTML = DATA.increasePrice
    document.getElementById("adoptAB").innerHTML = DATA.autoBuyPrice
    document.getElementById("startDate").innerHTML = joinDate(DATA.postTime.toDate(), '-');
    document.getElementById("endDate").innerHTML = joinDate(DATA.endTime.toDate(), '-');
    document.getElementById("postTags").innerHTML = ''
    for (let i = 0; i < DATA.tags.length; i++) {
        var div = document.createElement("div")
        div.innerHTML = DATA.tags[i]
        document.getElementById("postTags").appendChild(div)
    }
    document.getElementById("adoptDescript").innerHTML = DATA.description
    function setImageUrl(name) { //put ImgName from firebase into 'name' parameter
        firebase.storage().ref(name).getDownloadURL().then(function(url) {            
            document.getElementById("adoptImg").src = url
        });
    }
    setImageUrl(DATA.mainImgName)
    document.getElementById("bid-list").innerHTML = ""
    for (let i = 0; i < DATA.bid.length; i++) {
        console.log(DATA.bid[i].date.toDate())
        document.getElementById("bid-list").prepend(createBidList(DATA.bid[i].uid, DATA.bid[i].bidPrice, DATA.bid[i].date.toDate()))
    }

    //set remaining time
    calculateExamRemainingTime(DATA.endTime.toDate())

    //set follow button
    checkFollowing()

    //disable bid input
    if (DATA.bid.length != 0) {
        if (DATA.bid[DATA.bid.length - 1].bidPrice >= DATA.autoBuyPrice || DATA.endTime.toDate() < new Date()) {
            document.getElementById("bidPrice").disabled = true
            document.getElementById("bidButton").disabled = true
        }
    } 
    if (DATA.endTime.toDate() < new Date()) {
        document.getElementById("bidPrice").disabled = true
        document.getElementById("bidButton").disabled = true
    }
});

function checkDisableBid() {
    if (DATA.bid.length != 0) {
        if (DATA.bid[DATA.bid.length - 1].bidPrice >= DATA.autoBuyPrice || DATA.endTime.toDate() < new Date()) {
            document.getElementById("showtime").innerHTML = "Auction End"
            document.getElementById("bidPrice").disabled = true
            document.getElementById("bidButton").disabled = true
        }
    }
    if (DATA.endTime.toDate() < new Date()) {
        document.getElementById("showtime").innerHTML = "Auction End"
        document.getElementById("bidPrice").disabled = true
        document.getElementById("bidButton").disabled = true
    }
}

function createBidList(name, price, date) {
    let div = document.createElement("div")
    let dateFormat = joinDate(date, '-')
    firestore.collection("UserProfile").doc(name).get().then(function(docs){
        var profileData = docs.data()
        div.innerHTML = dateFormat + " <br> " + profileData.username + " : " + price + " ฿"
    })
    div.classList.add("bid-list-item")
    return div
}

function joinDate(t, s) { //set formate from Date
    let month
    switch (t.getMonth()) {
        case 0:
            month = "January"
            break;
        case 1:
            month = "February"
            break;
        case 2:
            month = "March"
            break;
        case 3:
            month = "April"
            break;
        case 4:
            month = "May"
            break;
        case 5:
            month = "June"
            break;
        case 6:
            month = "July"
            break;
        case 7:
            month = "August"
            break;
        case 8:
            month = "September"
            break;
        case 9:
            month = "October"
            break;
        case 10:
            month = "November"
            break;
        case 11:
            month = "December"
            break;
        default:
            month = "monthError"
            break;
    }
    return t.getDate() + s + month + s + t.getFullYear() + ", " + t.getHours()+":"+t.getMinutes()+":"+t.getSeconds()
 }

function bidding() {
    if (current_user === '') {
        window.location.href = "/public/login.html";
        return
    }
    let errorMessage = ''
    let currentBidPrice = Number(document.getElementById("bidPrice").value)
    if (Number.isNaN(currentBidPrice) || document.getElementById("bidPrice").value.match("e")) {
        errorMessage = errorMessage + 'Please insert valid price<br>'
    }
     if (!DATA.bid.length) {
        if (parseFloat(currentBidPrice) >= DATA.startingPrice) {
            updateBid()
        }else{
            errorMessage = errorMessage + 'Please bid more than starting price'
        }
     } else{
         if (parseFloat(currentBidPrice) < DATA.bid[DATA.bid.length - 1].bidPrice + DATA.increasePrice && parseFloat(currentBidPrice) < DATA.autoBuyPrice) {
            errorMessage = errorMessage + "Please bid more than last bid plus increase price"
         }
     }

     if (errorMessage === '') {
        updateBid()
     }
     document.getElementById("errorMessage").innerHTML = errorMessage

     document.getElementById("bidPrice").value = ''

     function updateBid() {
         let bidObj = {
             uid: current_user.uid,
             bidPrice: parseInt(currentBidPrice),
             date: firebase.firestore.Timestamp.fromDate(new Date())
         }
         console.log(DATA.bid)
         let myBidArray = [...DATA.bid]
         console.log(myBidArray)
         myBidArray.push(bidObj)
         console.log(myBidArray)
         firestore.collection("AdoptablePost").doc(postDocId).set({ 
            bid: myBidArray
        },{merge: true});
        updateFollowingPost()
     }
}
function checkFollowing() {
    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            current_user = user
            firestore.collection("UserProfile").doc(user.uid).get().then(function(docs){
                var profileData = docs.data()
                var tempArray = [...profileData.followingPost]
                if (tempArray.includes(postDocId)) {
                    document.getElementById("followButton").innerHTML = 'Unfollow'
                    document.getElementById("followButton").onclick = function() { unfollow(); }
                }
            })
        }
    })
}
function updateFollowingPost() {
    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            current_user = user
            firestore.collection("UserProfile").doc(user.uid).get().then(function(docs){
                var profileData = docs.data()
                var tempArray = [...profileData.followingPost]
                if (!tempArray.includes(postDocId)) {
                    tempArray.push(postDocId)
                    firestore.collection("UserProfile").doc(user.uid).set({ 
                        followingPost: tempArray
                    },{merge: true});
                    document.getElementById("followButton").innerHTML = 'Unfollow'
                    document.getElementById("followButton").onclick = function() { unfollow(); }
                }
            })
        }
    })
}
function unfollow() {
    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            current_user = user
            firestore.collection("UserProfile").doc(user.uid).get().then(function(docs){
                var profileData = docs.data()
                var tempArray = [...profileData.followingPost]
                if (tempArray.includes(postDocId)) {
                    Array.prototype.remove = function() {
                        var what, a = arguments, L = a.length, ax;
                        while (L && this.length) {
                            what = a[--L];
                            while ((ax = this.indexOf(what)) !== -1) {
                                this.splice(ax, 1);
                            }
                        }
                        return this;
                    };
                    tempArray.remove(postDocId);
                    firestore.collection("UserProfile").doc(user.uid).set({ 
                        followingPost: tempArray
                    },{merge: true});
                }
            })
        }
    })
    document.getElementById("followButton").innerHTML = 'Follow'
    document.getElementById("followButton").onclick = function() { updateFollowingPost(); }
}

let current_user = ''
firebase.auth().onAuthStateChanged(user => {
    if (user) {
        current_user = user
        console.log('you are '+ current_user)
    }else{
        console.log('you do not login')
        document.getElementById("profileNav").innerHTML = 'LOGIN'
    }
});

function calculateExamRemainingTime(exam_end_at) {
    $(function(){

        const calcNewYear = setInterval(function(){

            const exam_ending_at    = exam_end_at;
            const current_time      = new Date();
           
            const totalSeconds     = Math.floor((exam_ending_at - (current_time))/1000);;
            const totalMinutes     = Math.floor(totalSeconds/60);
            const totalHours       = Math.floor(totalMinutes/60);
            const totalDays        = Math.floor(totalHours/24);

            const days = totalDays
            const hours   = totalHours - ( totalDays * 24 );
            const minutes = totalMinutes - ( totalDays * 24 * 60 ) - ( hours * 60 );
            const seconds = totalSeconds - ( totalDays * 24 * 60 * 60 ) - ( hours * 60 * 60 ) - ( minutes * 60 );

            //const examRemainingHoursSection = document.querySelector('#remainingHours');
            //const examRemainingMinutesSection = document.querySelector('#remainingMinutes');
            //const examRemainingSecondsSection = document.querySelector('#remainingSeconds');

            const examRemainingDaysSection = days.toString()
            const examRemainingHoursSection = hours.toString();
            const examRemainingMinutesSection = minutes.toString();
            const examRemainingSecondsSection = seconds.toString();
            if (exam_ending_at <= current_time) {
                document.getElementById("showtime").innerHTML = "Auction End"
                document.getElementById("bidPrice").disabled = true
                document.getElementById("bidButton").disabled = true
            }else {
                document.getElementById("showtime").innerHTML = examRemainingDaysSection + " Days " + examRemainingHoursSection + " Hours " + examRemainingMinutesSection + " Minutes " + examRemainingSecondsSection + " Seconds"
            }
            checkDisableBid()
            

        },1000);
    });
}