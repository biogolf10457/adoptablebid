function getImageUrl(name) { //put ImgName from firebase into 'name' parameter
    let imgUrl
    firebase.storage().ref(name).getDownloadURL().then(function(url) {            
        imgUrl = url
        console.log('link is '+imgUrl)
    });
    return imgUrl //ถ้า imgUrl โยนออกนอกฟังก์ชันแล้วเป็น undefined, ให้เขียนการกระทำในฟังก์ชันนี้เลย
}