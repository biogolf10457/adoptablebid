let firestore = firebase.firestore()
let fbTags = firestore.collection("Tags")
let colorsTags
let genderTags
let themesTags
let uploading = false
fbTags.doc("colors").get().then(function(docs){ //get colors tags from firestore
    colorsTags = docs.data();
    addChoices('colors-choices', colorsTags) //add elements into HTML
})
fbTags.doc("gender").get().then(function(docs){ //get gender tags from firestore
    genderTags = docs.data();
    addChoices('gender-choices', genderTags) //add elements into HTML
})
fbTags.doc("themes").get().then(function(docs){ //get themes tags from firestore
    themesTags = docs.data();
    addChoices('themes-choices', themesTags) //add elements into HTML
})

function openChoice(id) { //open tags choice in form by each id

    //open parent div
    let choicesForm = document.getElementById("tags-choices-form")
    choicesForm.classList.remove("close")
    choicesForm.classList.add("open")

    //set id choices and open choices by id
    let choicesId = ['colors-choices', 'gender-choices', 'themes-choices'] 
    choicesId.forEach (cId => {
        if (cId === id) {
            document.getElementById(cId).classList.remove("close")
            document.getElementById(cId).classList.add("open")
            document.getElementById(cId.split("-")[0] + "-nav").classList.add("tags-nav-item-open")
        } else{
            document.getElementById(cId).classList.remove("open")
            document.getElementById(cId).classList.add("close")
            document.getElementById(cId.split("-")[0] + "-nav").classList.remove("tags-nav-item-open")
        }
    })
}

function addChoices(id, choices) { //render tag choice into html
    let name = ''
    if (id === 'colors-choices') {
        name = 'colors'
    } else if(id === 'gender-choices'){
        name = 'gender'
    } else if(id === 'themes-choices'){
        name = 'themes'
    }
    choices[name].forEach(tag => {
        choice = document.createElement('div')
        choice.id = 'tag_'+tag
        choice.setAttribute("onclick", `toggleSelect("${choice.id}")`)
        choice.classList.add('unselect')
        choice.setAttribute('type', 'button')
        choice.innerHTML = tag
        document.getElementById(id).appendChild(choice)
    });
}

var selectedTags = []
function toggleSelect(id) {
    let tagElement = document.getElementById(id)
    let tagName = id.split('_')[1]
    if (tagElement.classList.contains('unselect')) { //select tag
        tagElement.classList.remove('unselect')
        tagElement.classList.add('select')
        selectedTags.push(tagName)
    } else if(tagElement.classList.contains('select')){ //unselect tag
        tagElement.classList.remove('select')
        tagElement.classList.add('unselect')
        selectedTags = removeA(selectedTags, tagName)
    }
}

function removeA(arr) { //remove element in array by value
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}

//function for random collection id
function randomCollectionId() {
    let alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('')
    let number = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    let alphabetId = randomArray(alphabet) + randomArray(alphabet) + randomArray(alphabet) + randomArray(alphabet) + randomArray(alphabet)
    let numberId = randomArray(number) + randomArray(number) + randomArray(number) + randomArray(number) + randomArray(number) + randomArray(number)
    return alphabetId + numberId
}

function randomArray(array) {
    let randomIndex = Math.floor(Math.random() * array.length)
    let randomResult = ""+array[randomIndex]
    return randomResult
}

var file
$("#file_upload").on("change",function(event){ //store upload image to variable and preview the image at front-end
    file=event.target.files[0]

    if(file==null)//ถ้า object file เป็น null ให้ ออกจาก function
    {
        return
    }

    var reader  = new FileReader() //for image front-end preview
    reader.onload = function(e)  {
        var image = document.createElement("img")
        // the result image data
        image.src = e.target.result;
        document.getElementById("previewImg").innerHTML = ""
        document.getElementById("previewImg").appendChild(image)
        document.getElementsByClassName("imgForm")[0].style.height = 'fit-content'
     }
     // you have to declare the file loading
     reader.readAsDataURL(file)
})

function addPost() {
    //setup for upload image
    var uploadTask
    var img01Url
    var errorMessage = ''
    function addImageToStorage(imgFile, name) {
        var file_ref=firebase.storage().ref(name);//สร้าง file ใน firebase
        uploadTask = file_ref.put(imgFile);// upload file เข้าไปใน firebase
        uploadTask.on("state_changed",
            function progress(snapshot){//แสดง percent ความก้าวหน้าเมื่อ upload

            },
            function err(err){ //กำหนดชุดคำสั่งให้ทำงานเมื่อ upload  error

            },
            function complete(){ //กำหนดชุดคำสั่ง เมื่อ Upload เสร็จแล้ว
                img01Url = name
                console.log('upload complete')
                if (errorMessage !== '') {
                    showError()
                }else{
                    if (!uploading) {
                        updateToFirestore()
                    }
                }
            },
        )
    }
    function getImageUrl(name) { 
        let imgUrl
        firebase.storage().ref(name).getDownloadURL().then(function(url) {            
            imgUrl = url
            console.log('link is '+imgUrl)
        });
        return imgUrl //cannot return img Url outside function, it will be undefined
    }

    //set up base variable
    var collectionId = randomCollectionId(); console.log(collectionId);
    var autoBuyPrice = parseInt(document.getElementById("autoBuyPrice").value);
    var bid = [];
    var creator = current_user.uid;
    var description = document.getElementById("description").value;
    description = description.replace(/\n\r?/g, '<br />');
    var duration = parseInt(document.getElementById("duration").value);
    var increasePrice = parseInt(document.getElementById("increasePrice").value);
    var adoptableName = document.getElementById("name").value;
    var owner = current_user.uid;
    var postTime = firebase.firestore.Timestamp.fromDate(new Date());
    //set endTime
    var presentDate = new Date();
    var endTime = firebase.firestore.Timestamp.fromMillis(presentDate.setDate(presentDate.getDate() + duration));
    var startingPrice = parseInt(document.getElementById("startingPrice").value);
    var status = "active";
    var mainImgUrl = img01Url;
    var subImgUrl1 = "";
    var subImgUrl2 = "";
    var subImgUrl3 = "";
    var tags = selectedTags;

    //check condition
    if (adoptableName === "" || description === "" || autoBuyPrice === "" || increasePrice === "" || startingPrice === "" || duration == 0) {
        errorMessage = errorMessage + "Please insert all information before publish<br>"
    }
    if (autoBuyPrice < 0 || startingPrice < 0 || increasePrice < 0 || Number.isNaN(increasePrice) || Number.isNaN(startingPrice) || Number.isNaN(autoBuyPrice) ) {
        errorMessage = errorMessage + "Please insert valid price<br>"
    }
    if (autoBuyPrice <= startingPrice) {
        errorMessage = errorMessage + "Please make Auto Buy Price more than Starting Price<br>"
    }
    if (file != null) { //check if image was uploaded
        addImageToStorage(file, collectionId+'_img01')
    } else{
        errorMessage = errorMessage + "Please upload image<br>"
        showError()
    }

    function showError() {
        document.getElementById("errorMessage").innerHTML = errorMessage
    }
    
    function updateToFirestore() {
        var postObj = {
            autoBuyPrice: autoBuyPrice,
            bid: bid,
            creator: creator,
            description: description,
            duration: duration,
            endTime: endTime,
            increasePrice: increasePrice,
            mainImgName: img01Url,
            name: adoptableName,
            owner: owner,
            postTime: postTime,
            startingPrice: startingPrice,
            status: status,
            subImgName1: subImgUrl1,
            subImgName2: subImgUrl2,
            subImgName3: subImgUrl3,
            tags: tags
        };
        console.log(postObj)
        uploading = true
        firestore.collection("AdoptablePost").doc(collectionId).set(postObj);
        console.log('finish post')
        sessionStorage.setItem("postDocId", collectionId);
        checkDocUploadFinish()
        function checkDocUploadFinish() {
            setInterval(function(){
                firestore.collection("AdoptablePost").doc(collectionId).get().then(function(docs){
                    if(docs.exists){
                        console.log(docs.data());
                        window.location.href = "auctionPost.html";
                    } else {
                        console.log("No data");
                    }
                })
            }, 1000);
        }
    }
}
